package myprojects.lection3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class GoodsPage {

    private EventFiringWebDriver driver;


    Random rnd = new Random();
    private By productname = By.xpath("//input[@id='form_step1_name_1']");
    private By productQty = By.xpath("//input[@id='form_step1_qty_0_shortcut']");
    private By productPrice = By.id("form_step1_price_shortcut");
    private By productActivation = By.cssSelector(".switch-input");
    private By confirmPopup = By.cssSelector(".growl-message");
    private By closePopup = By.cssSelector(".growl-close");
    private By createButton = By.cssSelector(".btn.btn-primary.js-btn-save");

    public GoodsPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }


    public String fillProductName() {
        Random rnd = new Random();
        String productName = "test" + rnd.nextInt(100000);

        driver.findElement(productname).sendKeys(productName);
        return productName;
    }

    public void fillProductQty() throws InterruptedException {
        Random rnd = new Random();
        int productqty = rnd.nextInt(101);
        driver.findElement(productQty).sendKeys(Keys.chord(Keys.CONTROL, "a"), String.valueOf(productqty));
        Thread.sleep(500);

    }

    public int fillProductPrice() {
        Random rnd = new Random();
        int price = rnd.nextInt(101);
        driver.findElement(productPrice).sendKeys(Keys.chord(Keys.CONTROL, "a"), String.valueOf(price));
        return price;


    }

    public void activateProduct() throws InterruptedException {

        if (!driver.findElement(productActivation).isSelected()) {

            driver.findElement(productActivation).click();
        }

    }

    public void checkPopup() throws InterruptedException {
        driver.findElement(confirmPopup);
        driver.findElement(closePopup).click();
        Thread.sleep(500);
    }

    public void saveProduct() {
        driver.findElement(createButton).click();
    }


}
