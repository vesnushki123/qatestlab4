package myprojects.lection3.pages;

import myprojects.lection3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class LoginPage {
    private EventFiringWebDriver driver;
    private By emailInput = By.id("email");
    private By passInput = By.id("passwd");
    private By loginBtn = By.name("submitLogin");
    private By mainPage = By.id("main");


    public LoginPage(EventFiringWebDriver driver)
    {
        this.driver = driver;
    }

    public void open()
    {

        driver.get(Properties.getBaseAdminUrl());
    }

    public void fillEmailInput(String email)
    {
        driver.findElement(emailInput).sendKeys(email);
    }
    public void fillPassInput(String password)
    {
        driver.findElement(passInput).sendKeys(password);
    }
    public void clickLoginBtn()
    {
        driver.findElement(loginBtn).click();
    }
    public WebElement checkPage(){
       return  driver.findElement(mainPage);
    }
}
