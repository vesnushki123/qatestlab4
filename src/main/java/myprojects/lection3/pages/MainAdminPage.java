package myprojects.lection3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainAdminPage {

    private EventFiringWebDriver driver;
    private By catalogTab = By.id("subtab-AdminCatalog");
    private By addBtn = By.id("page-header-desc-configuration-add");


    public MainAdminPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void selectCatalogItem() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogTab));
        WebElement catalogTabElement = driver.findElement(catalogTab);
        Actions actions = new Actions(driver);
        actions.moveToElement(catalogTabElement).build().perform();
        catalogTabElement.findElements(By.cssSelector("li")).get(0).click();
    }
    public void clickOnAddBtn() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(addBtn));
        driver.findElement(addBtn).click();
    }

}
