package myprojects.lection3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ProductPage {

    private EventFiringWebDriver driver;
    private WebElement productPrice;
    private WebElement productQty;

    public ProductPage (EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public WebElement getProductQty(String qty) {
        By productQty = By.xpath("//div[@class='current-price']//span[contains(text(), '" + qty + "')]");
        return driver.findElement(productQty);
    }

    public WebElement getProductPrice(int price) {
        By productPrice = By.xpath("//div[@class='current-price']//span[contains(text(), '" + price + "')]");

        return driver.findElement(productPrice);
    }




}
