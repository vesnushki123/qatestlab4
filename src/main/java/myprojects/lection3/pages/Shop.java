package myprojects.lection3.pages;

import myprojects.lection3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.List;

import static myprojects.lection3.BaseTest.getConfiguredDriver;

public class Shop {
    private static EventFiringWebDriver driver;
    public WebElement productTitle;

    public WebElement getProductTitle(String productName) {
        By productTitle = By.xpath("//h1[@class='h3 product-title']/a[.='" + productName + "']");
        return driver.findElement(productTitle);
    }
//
//    public void setProductTitle(WebElement productTitle) {
//        this.productTitle = productTitle;
//    }


    public Shop(EventFiringWebDriver driver) {
        this.driver = driver;
    }


    public void openShop() {

        driver.get(Properties.getBaseShopUrl());
    }



}
