package myprojects.lection3.tests;

import myprojects.lection3.BaseTest;
import myprojects.lection3.pages.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class MyTest extends BaseTest {

    EventFiringWebDriver driver = getConfiguredDriver();
    String productName;
    int price;

    public int getPrice() {
        return price;
    }


    public String getProductName() {

        return productName;
    }


    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }


    @DataProvider
    public static Object[][] credentials() {
        return new Object[][]{{"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}};
    }


    @Test(dataProvider = "credentials")
    public void userLogin(String email, String password) throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        login.open();
        login.fillEmailInput(email);
        login.fillPassInput(password);
        login.clickLoginBtn();
        WebElement expected = driver.findElement(By.id("main"));
        Assert.assertEquals(expected, login.checkPage());

    }

    @Test(dependsOnMethods = "userLogin")
    public void createNewProduct() throws InterruptedException {

        MainAdminPage mainAdminPage = new MainAdminPage(driver);
        mainAdminPage.selectCatalogItem();
        mainAdminPage.clickOnAddBtn();
        GoodsPage goodsPage = new GoodsPage(driver);
        productName = goodsPage.fillProductName();
        goodsPage.fillProductQty();
        price = goodsPage.fillProductPrice();
        goodsPage.activateProduct();
        goodsPage.checkPopup();
        goodsPage.saveProduct();
        goodsPage.checkPopup();
    }


    @Test(dependsOnMethods = "createNewProduct")
    public void checkNewProduct() throws InterruptedException {
        Shop sh = new Shop(driver);
        ProductPage pp = new ProductPage(driver);
        sh.openShop();
        Assert.assertTrue(sh.getProductTitle(productName).isDisplayed());
        sh.getProductTitle(productName).click();
        Assert.assertTrue(pp.getProductPrice(price).isDisplayed());

    }


}
