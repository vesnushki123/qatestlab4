package myprojects.lection3.utils;

import org.omg.CORBA.Environment;
import org.openqa.selenium.remote.BrowserType;

public class Properties {
    private static final String DEFAULT_BASE_ADMIN_URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    private static final String DEAFULT_BROWSER = BrowserType.CHROME;
    private static final String DEFAULT_BASE_SHOP_URL = "http://prestashop-automation.qatestlab.com.ua/ru/2-home?page=2";

    public static String getBaseAdminUrl()
    {
        return System.getProperty(EnvironmentVariables.BROWSER.toString(), DEFAULT_BASE_ADMIN_URL);
    }
    public static String getBaseShopUrl()
    {
        return System.getProperty(EnvironmentVariables.BROWSER.toString(), DEFAULT_BASE_SHOP_URL);
    }
    public static String getBrowser()
    {
        return System.getProperty(EnvironmentVariables.BROWSER.toString(), DEAFULT_BROWSER);
    }
}

enum EnvironmentVariables{
    BASE_ADMIN_URL("env.admin.url"),
    BASE_SHOP_URL("http://prestashop-automation.qatestlab.com.ua/"),
    BROWSER("browser");

    private String value;
    EnvironmentVariables(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
